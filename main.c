
/****************************************************************************
 *
 *                        D�pro �lectronique Inc.
 *                       755 Cur� Boivin, suite 101
 *                        Boisbriand, QC, Canada
 *
 *                (c) Copyright 2008, D�pro �lectronique Inc.
 *
 * Tous droits r�serv�s. Ce fichier est une propri�t� intellectuelle
 * et confidentielle de D�pro �lectronique Inc. Toute tentative
 * d'appropriation de ce fichier est strictement interdite sans le
 * consentement explicite de D�pro �lectronique.
 *
 * Projet :         L00331
 * Fichier :        main.h
 * Programmeur(S) : Stephane Pigeon
 *                  
 * Date :           2022/01/29
 * IDE :            WinAVR 20100110
 *                 
 * Description :    
 *
 *  Source principal
 *
 *  Ce logiciel est le bootloader pour le bloc d'alimentation et la Cell
 *  Interface, pour Calisto-F de Morgan Schaffer. 
 * 
 * ---------------------------------------------------------------------------
 *
 * Les "fuse bits" du ATmega88 doivent etre programmees de la facon suivante pour 
 * que le logiciel fonctionne correctement:
 *
 * BOOTSZ = 00      Taille de 1 Kw, 0xC00 - 0xFFF
 * BOOTRST = 0      Permet le reset du ATmega88 dans la section BOOT
 * BODLEVEL = 100   Brown-out level at VCC=4.3V
 * Lock bits        Aucune restriction sur la section APPLICATION
 *                  SPM restreinte sur la section BOOT
 *
 * Avant de permettre les interruptions, le bit IVSEL doit etre a "0" afin de 
 * rediriger le vecteur d'interruption vers la section APPLICATION
 *
 * ----------------------------------------------------------------------------
 *
 * Carte memoire de la FLASH du Mega168 (adressage par mot de 16 bits)
 *
 * 0x0000               Non utilisee
 * 0x0001               Vecteur d'interruption si IVSEL = 0 (APPLICATION)
 * 0x0015               Logiciel d'APPLICATION
 * 0x1BFF               Fin du logiciel d'APPLICATION
 * ///
 * 0x1C00               Point de reset du Mega168
 * 0x1C01               Vecteur d'interruption si IVSEL = 1 (BOOT)
 * 0x1C15               Logiciel BOOTLOADER
 * 0x1FFF               Fin du logiciel BOOTLOADER
 * 
 ****************************************************************************/

/****************************************************************************
 *                                  INCLUDES
 ****************************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include "main.h"
#include "cmdset.h"
#include "crc.h"
#include "SelfProg.h"

/****************************************************************************
 *                                PUBLIC CONSTANTS
 ****************************************************************************/

/****************************************************************************
 *                                  DATA TYPES
 ****************************************************************************/

/****************************************************************************
 *                                  PROTOTYPES
 ****************************************************************************/

/****************************************************************************
 *                                GLOBAL VARIABLEs
 ****************************************************************************/

//unsigned char bHardReset;                   // Hard reset or soft reset?
unsigned char StayInBoot;                   // Stay in boot code (don't run application)

/****************************************************************************
 *                                PRIVATE VARIABLEs
 ****************************************************************************/



/*************************************************************
 * Desc:    Initialize the variables and the various resources
 * In:      None
 * Out:     None
 ************************************************************/

void Initialization ( void )
{
//sIPC_space *pIPC_flags = (sIPC_space *)(LAST_LOCATION_IN_RAM - sizeof(sIPC_space) + 1);

    // Determine hard reset or soft reset by the state of the watchdog.
    // If this is a hardware reset (and the watchdog is not automatically
    // enabled by the fuse map), the watchdog will not be enabled.
    //bHardReset = ( WDTCSR & (1 << WDE) ) ? FALSE : TRUE;
    //StayInBoot = !bHardReset;

    // Determine what type of reset got us here
    // If MCUSR is equal to 0, then there was a jump in the bootloader
    // so stay here.  All other reset type (POWERON, BROWNOUT, WD and
    // EXTERNAL) must try to launch application.
    // bit 0 : POWERON_RESET
    // bit 1 : EXTERNAL_RESET
    // bit 2 : BROWNOUT_RESET
    // bit 3 : WATCHDOG_RESET
    // others: unused (0)
    StayInBoot = MCUSR ? FALSE : TRUE;
    
    // Reset the different flags if we want to be able to discreminate
    // what kind of reset occured next time
    MCUSR = 0;
    
    // Enable the watchdog
    // Watchdog uses a separate 128 KHz crystal so the timing selected
    // always applies, regardless of the main MCU xtal. We could simply 
    // use WDTON in the fuse map to force the watchdog in system reset 
    // mode, but doing it explicitely is safer.
    // Always reset the watchdog timer in case we change to a shorter 
    // prescaler and a system reset occurs right away !
    asm volatile ( "wdr" );
    // Clear WDRF in MCUSR, cause if raised it overwrites WDE in WDTCSR
    MCUSR &= ~( 1 << WDRF );
    // Any change to WDT requires Writing 1 to WDCE and WDE in WDTCSR
    WDTCSR |= ( 1 << WDCE ) | ( 1 << WDE );
    // Now set the desired config within the next 4 cycles
    // WDID = 0, interrupt flag cleared
    // WDIE = 0, no interrupt (system reset used instead)
    // WDE = 0/1, Watchdog disabled/enabled 
    // WDP3/2/1/0 = 0011, 125 msec prescaler
    WDTCSR = ( 1 << WDE ) | ( 1 << WDP1 ) | ( 1 << WDP0 );

    // Enable change of interrupt vectors
    MCUCR = (1 << IVCE);
    // Move interrupts to boot flash section
    MCUCR = (1 << IVSEL);
  
    // Disable interrupts
    cli();
    
    // Initialize the commandset interface handler
    cmdset_Init();
    
    // Initialize the external DAC handler
    // dac_Init();
    
    // Initialize the adc handler
    // adc_Init();

    /////////////////////////////////////////
    // Specific to the Cell Interface
    /////////////////////////////////////////

    // PORTB
    //CBI ( DDRB, AD24_READY );           // AD24_READY = INPUT
    //CBI ( PORTB, CELL_HEATER_PWM );     // CELL_HEATER_PWM = OUTPUT 0
    //SBI ( DDRB, CELL_HEATER_PWM );        
    // PB2,3,4,5 init by SPI Interface
    CBI ( DDRB, 6 );                    // XTAL1 = INPUT
    CBI ( DDRB, 7 );                    // XTAL2 = INPUT

    // PORTC
    DDRC = 0x00;                        // All inputs, even D_CATHAL_BURN_EN and 2-wire interface

    // PORTD
    //CBI ( PORTD, CELL_VALVE1_EN );      // CELL_VALVE1_EN = OUTPUT 0
    //SBI ( DDRD, CELL_VALVE1_EN );
    //CBI ( DDRD, D_VALVE1_STAT );        // D_VALVE1_STAT = INPUT
    //CBI ( PORTD, CELL_VALVE2_EN );      // CELL_VALVE2_EN = OUTPUT 0
    //SBI ( DDRD, CELL_VALVE2_EN );
    //CBI ( PORTD, CATAL_BURNER_PWM );    // CATAL_BURNER_PWM = OUTPUT 0
    //SBI ( DDRD, CATAL_BURNER_PWM );
    //CBI ( DDRD, D_VALVE2_STAT );        // D_VALVE2_STAT = INPUT
    //CBI ( PORTD, VALVE_PWM );           // VALVE_PWM = OUTPUT 0
    //SBI ( DDRD, VALVE_PWM );
    //CBI ( PORTD, PLATE_HEATER_PWM );    // PLATE_HEATER_PWM = OUTPUT 0
    //SBI ( DDRD, PLATE_HEATER_PWM );
    //SBI ( PORTD, DEBUG_LED );           // DEBUG_LED = OUTPUT 1
    //SBI ( DDRD, DEBUG_LED );

    // Enable interrupts
    sei();
}

/*************************************************************
 * Desc:    Call up the main application
 *          The main application must contain a jump to its 
 *          startup code at address 0x0000
 * In:      None
 * Out:     None
 ************************************************************/

void StartApp ( void )
{
    uint16_t crc, Start, End, ncrc;
    uint8_t i;
    char sig[ASSEMBLY_SIGNATURE_SIZE+1];
    
    // Check the application's CRC first

    // Load crc, StartAddr and EndAddr from Flash (page just below boot loader)
    crc = pgm_read_word ( BOOTCODE_ADDR - SPM_PAGESIZE );
    Start = pgm_read_word ( (BOOTCODE_ADDR - SPM_PAGESIZE) + 2 );
    End = pgm_read_word ( (BOOTCODE_ADDR - SPM_PAGESIZE) + 4 );
    
    // Recompute the crc and compare
    ncrc = MakeCRC ( Start, End );
    if ( ncrc != crc ) {
Error:    
        cmdset_SendReply ( "ERROR" );
        return;
    }

    // Check the application's assembly signature
    strcpy_P ( sig, ASSEMBLY_SIGNATURE );
    for ( i=0; i<ASSEMBLY_SIGNATURE_SIZE; i++ ) {
        if ( pgm_read_byte ( End - ASSEMBLY_SIGNATURE_SIZE + i ) != sig[i] ) goto Error;
    }
    
    // Jump to application
    // Deactivate interrupt sources
    SPCR = 0;
    // Disable interrupts
    cli();
    // Enable change of interrupt vectors
    MCUCR = (1 << IVCE);
    // Move interrupts to application section
    MCUCR = 0;
    // Jump to application
    asm volatile ( " jmp 0x0000" );
    // Old code for smaller ATmegas that don't support absolute jumps
    // asm volatile ( " ldi r16,0" );   
    // asm volatile ( " push r16" );
    // asm volatile ( " push r16" );
    // asm volatile ( " ret" );
}

/*************************************************************
 * Desc:    Program's entry point
 * In:      None
 * Out:     None
************************************************************/

int main ( void )
{
    uint32_t delay;
    
    delay = 0;
    StayInBoot = FALSE; 
    Initialization ();

    //CBI ( PORTD, DEBUG_LED );

    while (TRUE) {

        // Feed the watchdog at the very least once every 125 ms
        // ISR routines that don't last anywhere near 125 ms
        asm volatile ( "wdr" );

        // Handle incoming commands from the master
        cmdset_Interface ();

        // 1 second after bootup, start the main application, unless we have received the BOOT command
        // We do it this way, which is rather ugly, but it saves us from having to set up a timer ISR
        // We are very short on code space!
        if ( delay < 150000L )
        {
            delay++;
            if ( delay == 150000L ) 
            {
                if ( !StayInBoot ) {
                    StartApp();
                    StayInBoot = TRUE;
                }
            }
        }

    }

    // We'll never get here, this simply avoids a compiler warning
    return 1;
}


