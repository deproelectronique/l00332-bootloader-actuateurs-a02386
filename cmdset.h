
/****************************************************************************
 *
 *                        D�pro �lectronique Inc.
 *                       755 Cur� Boivin, suite 101
 *                        Boisbriand, QC, Canada
 *
 *                (c) Copyright 2008, D�pro �lectronique Inc.
 *
 * Tous droits r�serv�s. Ce fichier est une propri�t� intellectuelle
 * et confidentielle de D�pro �lectronique Inc. Toute tentative
 * d'appropriation de ce fichier est strictement interdite sans le
 * consentement explicite de D�pro �lectronique.
 *
 * Projet :         L00259-BIN-R2.0.2
 *                  L00261-BIN-R2.0.2
 * Fichier :        cmdset.h
 * Programmeur(S) : Stephane Pigeon
 *                  
 * Date :           2008/09/30
 * IDE :            WinAVR 20071221
 *                 
 * Description :    
 *	
 *  Definitions pour cmdset.c
 *
 *  Ce logiciel est le bootloader pour le bloc d'alimentation d�velopp� 
 *  � l'interne par D�pro �lectronique pour Calisto-F de Morgan Schaffer. 
 * 
 ****************************************************************************/

#define	ASCII_LF            10
#define	ASCII_CR            13

void cmdset_ResetSPI ( void );
void cmdset_Init ( void );
void cmdset_Interface ( void );
void cmdset_SendReply ( char *cmd );

