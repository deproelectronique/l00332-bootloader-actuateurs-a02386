
/****************************************************************************
 *
 *                        D�pro �lectronique Inc.
 *                       755 Cur� Boivin, suite 101
 *                        Boisbriand, QC, Canada
 *
 *                (c) Copyright 2008, D�pro �lectronique Inc.
 *
 * Tous droits r�serv�s. Ce fichier est une propri�t� intellectuelle
 * et confidentielle de D�pro �lectronique Inc. Toute tentative
 * d'appropriation de ce fichier est strictement interdite sans le
 * consentement explicite de D�pro �lectronique.
 *
 * Projet :         L00259-BIN-R2.0.2
 *                  L00261-BIN-R2.0.2
 * Fichier :        eeprom.c
 * Programmeur(S) : Stephane Pigeon
 *                  
 * Date :           2008/09/30
 * IDE :            WinAVR 20071221
 *                 
 * Description :    
 *
 *  Functions for using the on-chip EEPROM device
 *
 *  Memory map located in eeprom.h
 * 
 ****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>

#include "eeprom.h"

/*************************************************************
 * Desc:    Low level routines to read the EEPROM
 * In:      16 bit address into the EEPROM array
 * Out:     8-bit value read at requested address
 ************************************************************/

unsigned char EEPROM_read ( uint16_t uiAddress )
{
unsigned char cSREG;
uint8_t x;

    // Disable interrupts
    cSREG = SREG;
    cli();
    
    // Wait for completion of previous write
    while ( EECR & ( 1 << EEPE ) );
    // Set up address register
    EEAR = uiAddress;
    // Start eeprom read by writing EERE
    EECR = ( 1 << EERE );
    // Return data from Data Register
    x = EEDR;
    // Reset the EEPROM's control register
    EECR = 0x00;
    
    // Enable interrupts if they were enabled prior
    SREG = cSREG;
    return x;
}

/*************************************************************
 * Desc:    Low level routines to write the EEPROM
 * In:      16 bit address into the EEPROM array
 * Out:     8-bit value to write at requested address
 ************************************************************/

/*void EEPROM_write ( uint16_t uiAddress, uint8_t ucData )
{
unsigned char cSREG;

    // Disable interrupts
    cSREG = SREG;
    cli();

    // Wait for completion of previous write
    while ( EECR & ( 1 << EEPE ) );
    // Set up address and Data Registers
    EEAR = uiAddress;
    EEDR = ucData;
    // Write logical one to EEMPE (master program enable) while leaving EEPE at 0
    // EEPM1 and EEPM0 will be 0 to erase/write cell in a single operation
    // EERIE will be 0 for no interrupt
    // EEPE will be 0, which is the normal sequence
    // EERE will be 0 since we don't need to read the EEPROM
    EECR = ( 1 << EEMPE );
    // Start eeprom write by setting EEPE
    EECR |= ( 1 << EEPE );
    // Wait for completion of write
    while ( EECR & ( 1 << EEPE ) );
    // Reset the EEPROM's control register
    EECR = 0x00;

    // Enable interrupts if they were enabled prior
    SREG = cSREG;
}*/

