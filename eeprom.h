
/****************************************************************************
 *
 *                        D�pro �lectronique Inc.
 *                       755 Cur� Boivin, suite 101
 *                        Boisbriand, QC, Canada
 *
 *                (c) Copyright 2008, D�pro �lectronique Inc.
 *
 * Tous droits r�serv�s. Ce fichier est une propri�t� intellectuelle
 * et confidentielle de D�pro �lectronique Inc. Toute tentative
 * d'appropriation de ce fichier est strictement interdite sans le
 * consentement explicite de D�pro �lectronique.
 *
 * Projet :         L00259-BIN-R2.0.2
 *                  L00261-BIN-R2.0.2
 * Fichier :        eeprom.h
 * Programmeur(S) : Stephane Pigeon
 *                  
 * Date :           2008/09/30
 * IDE :            WinAVR 20071221
 *                 
 * Description :    
 *
 *  Definitions and prototypes for using the on-chip EEPROM device
 * 
 ****************************************************************************/

// Memory map

#define EEPROM_HWREV_ADDR   0
#define EEPROM_HREWV_SIZE   2

// Public functon prototypes 

unsigned char EEPROM_read ( uint16_t uiAddress );
// void EEPROM_write ( uint16_t uiAddress, uint8_t ucData );

