
/****************************************************************************
 *
 *                        D�pro �lectronique Inc.
 *                       755 Cur� Boivin, suite 101
 *                        Boisbriand, QC, Canada
 *
 *                (c) Copyright 2008, D�pro �lectronique Inc.
 *
 * Tous droits r�serv�s. Ce fichier est une propri�t� intellectuelle
 * et confidentielle de D�pro �lectronique Inc. Toute tentative
 * d'appropriation de ce fichier est strictement interdite sans le
 * consentement explicite de D�pro �lectronique.
 *
 * Projet :         L00259-BIN-R2.0.2
 *                  L00261-BIN-R2.0.2
 * Fichier :        main.h
 * Programmeur(S) : Stephane Pigeon
 *                  
 * Date :           2008/09/30
 * IDE :            WinAVR 20071221
 *                 
 * Description :    
 *
 *  Definitions pour main.c
 *
 *  Ce logiciel est le bootloader pour le bloc d'alimentation et la Cell
 *  Interface, pour Calisto-F de Morgan Schaffer. 
 * 
 ****************************************************************************/

/****************************************************************************
 *                               DATA TYPES
 ****************************************************************************/
// This structure holds the InterProcess Communication flag.  It allows
// the bootloader and application to exchange information through a
// shared memory.  Both the bootloader and application must declare this
// memory location to the end of internal RAM as follow:
// sIPC_space *pIPC_flags = (sIPC_space *)(LAST_LOCATION_IN_RAM - sizeof(sIPC_space) + 1);
// IMPORTANT NOTE: The stack pointer, that usually gets initialized at the
// top of the internal RAM, has been relocated to make room for this IPC
// structure.  If you change the struct SIZE, you must change the stack pointer
// location.  The stack pointer location is modified using a linker command
// line option in the Makefile (Wl,--defsym=__stack=0x008004F7)
typedef struct struct_IPC_space {
    uint32_t    signature;
    uint8_t     flags;
} sIPC_space;

/****************************************************************************
 *                            PUBLIC CONSTANTS
 ****************************************************************************/

#ifdef L00259
    // PSU Calisto2
    #define REV_STRING          PSTR("L00259-BIN-R2.0.2")
    #define ASSEMBLY_SIGNATURE  PSTR("A02185")
#else
#ifdef L00261
    // CELL 
    #define REV_STRING          PSTR("L00261-BIN-R2.0.2")
    #define ASSEMBLY_SIGNATURE  PSTR("A02158")
#else
    // PSU OMM
    #define REV_STRING          PSTR("L00275-BIN-R2.0.2")
    #define ASSEMBLY_SIGNATURE  PSTR("A02292")
#endif
#endif


#define FALSE                           0
#define TRUE                            (!FALSE)
#define OFF                             0
#define ON                              (!FALSE)
#define WATCHDOG_RESET                  3
#define BROWNOUT_RESET                  2
#define EXTERNAL_RESET                  1
#define POWERON_RESET                   0

#define LAST_LOCATION_IN_RAM            0x008004FF
#define IPC_SIGNATURE                   0xAA5500FF

// InterProcess Communication flags
/* DDP changed to support OMM PSU #ifdef L00259
    // PSU
    #define IPC_FLAG_13V_ON             0
#else
    // CELL
#endif
*/
#ifdef L00261
    // CELL
#else
    // BOTH PSU
    #define IPC_FLAG_13V_ON             0
    // CELL
#endif


#define SBI(p,b)    ((p) = (p)| (1<<(b)))
#define CBI(p,b)    ((p) = (p) & ~(1<<(b)))

#define TIMER0_FREQ         67

#define ASSEMBLY_SIGNATURE_SIZE     6

/****************************************************************************
 *                       ATmega168 PIN ASSIGNMENTS
 ****************************************************************************/

// PORTB
#define SSP0_CELL_CS                2   // PB2 digital output, SPI SLAVE SELECT
#define SPI_DATA_MOSI               3   // PB3 used for SPI interface
#define SPI_DATA_MISO               4   // PB4 used for SPI interface
#define SPI_SCK                     5   // PB5 used for SPI interface

// This section had to be reversed
#ifdef L00261                           // Specific to Cell

    // PORTB
    #define AD24_READY              0   // PB0 digital input
    #define CELL_HEATER_PWM         1   // PB1 digital output driven by Timer1 Fast PWM A
                                        // PB6 reserved for XTAL1, unavailable for I/O
                                        // PB7 reserved for XTAL2, unavailable for I/O

    // PORTC
    #define D_CATHAL_BURN_EN        0   // PC0 digital output
    #define CELL_HEATER_MEAS        1   // PC1 A/D input 
    #define CATALYTIC_BURNER_MEAS   2   // PC2 A/D input 
    #define CELL_VALVE_MEAS         3   // PC3 A/D input 
    #define EEPROM_SDA              4   // PC4 2-wire interface
    #define EEPROM_SCL              5   // PC5 2-wire interface
    #define ONEVREF                 6   // PC6 A/D input
    #define PLATE_HEATER_MEAS       7   // PC7 A/D input 

    // PORT D
    #define CELL_VALVE1_EN          0   // PD0 digital output
    #define D_VALVE1_STAT           1   // PD1 digital input
    #define CELL_VALVE2_EN          2   // PD2 digital output
    #define CATAL_BURNER_PWM        3   // PD3 digital output driven by Timer2 Fast PWM B
    #define D_VALVE2_STAT           4   // PD4 digital input
    #define VALVE_PWM               5   // PD5 digital output driven by Timer0 Fast PWM B
    #define PLATE_HEATER_PWM        6   // PD6 digital output driven by Timer0 Fast PWM A
    #define DEBUG_LED               7   // PD7 not used (can be used for a debug LED)

#else                                   // Specific to Both PSU, Most be reedited to account for differences

    // PORTB
    #define ZERO_DETECT             0   // PB0 digital input, ZERODETECT for APL Detection
    #define SLAVE_CLR               1   // PB1 digital output, SLAVE CLR for DAC converter
    #define SLAVE_MOSI              6   // PB6 digital output, SLAVE MOSI for DAC converter
    #define SLAVE_CLK               7   // PB7 digital output, SLAVE CLK for DAC converter

    // PORTC
    #define ADC_CH1_I               0   // PC0 A/D input CH1 current
    #define ADC_TEMP2               1   // PC1 A/D input temp limit 2
    #define ADC_TEMP_TRANSF         2   // PC2 A/D transformer temperature
    #define ADC_CH2_I               3   // PC3 A/D input CH2 current
    #define ADC_TEMP1               4   // PC4 A/D input temp limit 1
    #define ADC_TEMP_BOX            5   // PC5 A/D box temperature
    #define ADC_CH2_V               6   // PC6 A/D input CH2 voltage
    #define ADC_CH1_V               7   // PC7 A/D input CH1 voltage

    // PORT D
    #define SDO                     0   // PD0 used as the AD5307 daisy chain data out (not used)
    #define SLAVE_PD                1   // PD1 used as the AD5307 SLAVEPD signal (power down)
    #define CH1HEAT                 2   // PD2 digital output, CH1 HEAT
    #define CH1COOL                 3   // PD3 digital output, CH1 COOL
    #define CH2HEAT                 4   // PD4 digital output, CH2 HEAT
    #define CH2COOL                 5   // PD5 digital output, CH2 COOL
    #define V13_ON_OFF              6   // PD6 digital output, ON/OFF for 13.2V output
    #define SLAVE_CS                7   // PD7 digital output, SLAVE CS for DAC converter


#endif
                       

/****************************************************************************
 *                            FUNCTION PROTOTYPES
 ****************************************************************************/

void StartApp ( void );

