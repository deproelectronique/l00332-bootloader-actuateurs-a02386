
/****************************************************************************
 *
 *                        D�pro �lectronique Inc.
 *                       755 Cur� Boivin, suite 101
 *                        Boisbriand, QC, Canada
 *
 *                (c) Copyright 2008, D�pro �lectronique Inc.
 *
 * Tous droits r�serv�s. Ce fichier est une propri�t� intellectuelle
 * et confidentielle de D�pro �lectronique Inc. Toute tentative
 * d'appropriation de ce fichier est strictement interdite sans le
 * consentement explicite de D�pro �lectronique.
 *
 * Projet :         L00259-BIN-R2.0.2
 *                  L00261-BIN-R2.0.2
 * Fichier :        crc.c
 * Programmeur(S) : Stephane Pigeon
 *                  
 * Date :           2008/09/30
 * IDE :            WinAVR 20071221
 *                 
 * Description :    
 *        
 *  Functions to compute a 16-bit CCITT standard CRC
 * 
 ****************************************************************************/

#include <stdio.h>
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#include "crc.h"

//////////////////////////////////////////////////////////////
// Name: CRC16_CCITT_UPDATE
// Fnc:  Add a byte to the 16-bit CRC helf in crclo/crchi
//       CRC is CCITT standard polynomial 0x1021
//       crclo/crchi must be seeded at 0 prior to the first call
// Out:  Updated crc
//////////////////////////////////////////////////////////////

uint16_t CRC16_CCITT_Update ( uint16_t crc, uint8_t byte )
{
uint8_t i, carry;

    crc = ( ((crc >> 8) ^ byte) << 8 ) | ( crc & 0xFF );
    for ( i=0; i<8; i++ )
    {
        carry = ( ( crc & 0x8000 ) == 0x8000 );
        crc <<= 1;
        if ( carry ) crc ^= 0x1021;
    }
    return crc;
}
    
//////////////////////////////////////////////////////////////
// Name: MAKECRC
// Fnc:  Add a byte to the 16-bit CRC helf in crclo/crchi
//       CRC is CCITT standard polynomial 0x1021
//       crclo/crchi must be seeded at 0 prior to the first call
// Out:  Updated crc
//////////////////////////////////////////////////////////////

uint16_t MakeCRC ( uint16_t Start, uint16_t End )
{
uint16_t addr;
uint16_t crc;
unsigned char Temp;

    // Seed the CRC at 0
    crc = 0;
    for ( addr=Start; addr<End; addr++ )
    {
        Temp = pgm_read_byte ( addr );
        crc = CRC16_CCITT_Update ( crc, Temp );
        asm volatile ( " wdr" );
    }
    return crc;
}

