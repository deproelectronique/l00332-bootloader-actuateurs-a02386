
/****************************************************************************
 *
 *                        D�pro �lectronique Inc.
 *                       755 Cur� Boivin, suite 101
 *                        Boisbriand, QC, Canada
 *
 *                (c) Copyright 2008, D�pro �lectronique Inc.
 *
 * Tous droits r�serv�s. Ce fichier est une propri�t� intellectuelle
 * et confidentielle de D�pro �lectronique Inc. Toute tentative
 * d'appropriation de ce fichier est strictement interdite sans le
 * consentement explicite de D�pro �lectronique.
 *
 * Projet :         L00255-BIN-R2.0.2
 * Fichier :        adc.h
 * Programmeur(S) : Stephane Pigeon
 *                  
 * Date :           2008/10/01
 * IDE :            WinAVR 20071221
 *                 
 * Description :    
 *		
 *  Source principal : initialisation, main loop
 *
 *  Ce logiciel contr�le le bloc d'alimentation d�velopp� � l'interne par 
 *  D�pro �lectronique pour Calisto-F de Morgan Schaffer. L'objectif est
 *  de fournir une sortie 13.2V aux divers circuits composant Calisto-F,
 *  et de contr�ler 2 sorties de puissance pour des drivers Peltiers avec 
 *  des consignes en tension ou en courant.
 *
 *  C'est la carte CPU principale de Calisto-F qui envoie les consignes au
 *  logiciel du bloc d'alimentation, via une interface de contr�le SPI.
 * 
 ****************************************************************************/

void adc_Init(void);
char adc_Handle(uint16_t *adc_tempBox);

