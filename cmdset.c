
/****************************************************************************
 *
 *                        D�pro �lectronique Inc.
 *                       755 Cur� Boivin, suite 101
 *                        Boisbriand, QC, Canada
 *
 *                (c) Copyright 2008, D�pro �lectronique Inc.
 *
 * Tous droits r�serv�s. Ce fichier est une propri�t� intellectuelle
 * et confidentielle de D�pro �lectronique Inc. Toute tentative
 * d'appropriation de ce fichier est strictement interdite sans le
 * consentement explicite de D�pro �lectronique.
 *
 * Projet :         L00259-BIN-R2.0.2
 *                  L00261-BIN-R2.0.2
 * Fichier :        cmdset.h
 * Programmeur(S) : Stephane Pigeon
 *                  
 * Date :           2008/09/30
 * IDE :            WinAVR 20071221
 *                 
 * Description :    
 *  
 *  Fonctions pour le command set SPI
 *
 *  Ce logiciel est le bootloader pour le bloc d'alimentation d�velopp� 
 *  � l'interne par D�pro �lectronique pour Calisto-F de Morgan Schaffer. 
 * 
 ****************************************************************************/

/****************************************************************************
 *                                  INCLUDES
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include "main.h"
#include "cmdset.h"
#include "SelfProg.h"
#include "crc.h"
#include "eeprom.h"

/****************************************************************************
 *                                PRIVATE CONSTANTS
 ****************************************************************************/

#define MAX_RX_BUF          (SPM_PAGESIZE * 2 + 20)
#define MAX_TX_BUF          64

#define SPI_RECEIVING       0   // SPI ISR is in the 'receiving' mode
                                // The master is clocking out a command
#define SPI_BUSY            1   // SPI ISR is in the 'busy' mode
                                // We are preparing the response to a command
#define SPI_RESPONDING      2   // SPI ISR is in the 'responding' mode
                                // The master is clocking in our response

#define SPI_RESET_TIMEOUT   4       // 4 seconds to reset SPI if no SPI activity

/****************************************************************************
 *                                  DATA TYPES
 ****************************************************************************/

/****************************************************************************
 *                                  PROTOTYPES
 ****************************************************************************/

/****************************************************************************
 *                                GLOBAL VARIABLEs
 ****************************************************************************/

extern unsigned char StayInBoot;

/****************************************************************************
 *                                PRIVATE VARIABLEs
 ****************************************************************************/

static uint16_t RXBufInx;
static uint8_t TXBufInx;
static uint8_t Direction = SPI_RECEIVING;
static uint8_t CommandReady;

static char RXBuf[MAX_RX_BUF+1];
static char TXBuf[MAX_TX_BUF+1];
static char Reply[MAX_TX_BUF+1];

/****************************************************************************
 * Desc:    ISR routine for the SPI
 * In:      None
 * Out:     None
 ****************************************************************************/

SIGNAL ( SIG_SPI )
{
    static uint8_t Temp;

    ///////////////////////////////////////////////////////////////////
    // SPI Master is clocking out a command, we are clocking it in.
    ///////////////////////////////////////////////////////////////////
    
    if ( Direction == SPI_RECEIVING ) {

        // Receive the character from the SPI data register
        Temp = SPSR;
        Temp = SPDR;
        
        if ( Temp == 0 || Temp == ASCII_LF ) {
            // Just drop empty and LF characters
        }
        else {
            if ( Temp == ASCII_CR ) {
                // Full command received, warn cmdset_interface
                CommandReady = TRUE;
                RXBuf[RXBufInx] = 0;
                Direction = SPI_BUSY;
            }
            else {
                // Add valid character to incoming command string
                // RXBuf[RXBufInx++] = Temp;
                // GFOURNIER 2009-01-19 : Changed this line to uppercase right on here instead
                // of calling upstr() at the end.
                RXBuf[RXBufInx++] = (Temp >= 'a' && Temp <= 'z') ? Temp - ('a' - 'A') : Temp;
                if ( RXBufInx == MAX_RX_BUF ) RXBufInx = 0;
            }
        }

    }
    
    ///////////////////////////////////////////////////////////////////
    // We are busy preparing a response to a command
    // If the SPI master clocks in the response to soon we must send '@'
    ///////////////////////////////////////////////////////////////////

    if ( Direction == SPI_BUSY ) {
        SPDR = '@';
    }
    
    ///////////////////////////////////////////////////////////////////
    // SPI Master is clocking in our response (which is available)
    ///////////////////////////////////////////////////////////////////

    if ( Direction == SPI_RESPONDING ) {

        // Push the next character in the response
        Temp = TXBuf[TXBufInx++];
        SPDR = Temp;
        
        // When sending the last character (LF), go back to receiving mode
        if ( Temp == ASCII_LF || TXBufInx == MAX_TX_BUF ) {
            Direction = SPI_RECEIVING;
        }
    }

}

/*************************************************************
 * Desc:    Send reply to master
 * In:      Reply string
 * Out:     None
 ************************************************************/

void cmdset_SendReply ( char *cmd )
{
    uint8_t i;

    // Adding the reply and command trailer
    for ( i=0; cmd[i]; i++ ) {
        TXBuf[i] = cmd[i];
    }
    TXBuf[i++] = ASCII_CR;
    TXBuf[i++] = ASCII_LF;
  
    // Prepare transmission of formatted response
    TXBufInx = 0;
    Direction = SPI_RESPONDING;
}

/*************************************************************
 * Desc:    Functions to decode a nibble, 8 bits or 16 bits encoded in ASCII HEX
 * In:      
 * Out:     Decoded data
 ************************************************************/

static uint8_t DecodeNibble ( char c )
{
    uint8_t n;

    if ( c >= '0' && c <= '9' ) 
            n = c - '0';
    else    n = ( c - 'A' ) + 10;
    return n;
}

static uint8_t DecodeHex8 ( char *str )
{
    uint8_t i8;

    i8 = DecodeNibble ( str[0] );
    i8 = ( i8 << 4 ) | DecodeNibble ( str[1] );
    return i8;
}

static uint16_t DecodeHex16 ( char *str )
{
    uint16_t i16;

    i16 = DecodeHex8 ( &str[0] );
    i16 = ( i16 << 8 ) | DecodeHex8 ( &str[2] );
    return i16;
}


static unsigned char EncodeNibble ( unsigned char val )
{
	if ( val > 9 )
		val = ( val - 10 ) + 'A';
	else	val = val + '0';
	return val;
}

/*************************************************************
 * Desc:    Stay in the boot code, do not call the main application
 *          At startup, the boot loader gives a 1-second window of 
 *          opportunity to the PTE if it wants to remain in the bootcode, 
 *          to upload a new program
 * In:      Command string
 * Out:     None
 ************************************************************/

static void cmdBoot ( char *cmd )
{
    Direction = SPI_RECEIVING;
    // cmdset_SendReply ( "OK" );
}

/*************************************************************
 * Desc:    Read and transmit product ID from EEPROM, along
 *          with the firmware revision string and the assembly
 *          signature 
 * In:      Command string
 * Out:     None
 ************************************************************/

static void cmdReadPID ( char *cmd )
{
    uint8_t i, hwrev1, hwrev2;

    // Get both copies of the hardware revision from EEPROM
    hwrev1 = EEPROM_read ( EEPROM_HWREV_ADDR );
    hwrev2 = EEPROM_read ( EEPROM_HWREV_ADDR + 1 );

    // Reply = <FIRMWARE REVISION STRING>;<HARDWARE REVISION>;<ASSMEBLY SIGNATURE>
    strcpy_P ( Reply, REV_STRING );
    i = strlen ( Reply );
    Reply[i++] = ';';
    Reply[i++] = EncodeNibble ( hwrev1 >> 4 );
    Reply[i++] = EncodeNibble ( hwrev1 & 0x0F );
    Reply[i++] = EncodeNibble ( hwrev2 >> 4 );
    Reply[i++] = EncodeNibble ( hwrev2 & 0x0F );
    Reply[i++] = ';';
    strcpy_P ( &Reply[i], ASSEMBLY_SIGNATURE );
    
    cmdset_SendReply ( Reply );
}

/*************************************************************
 * Desc:    Write a block of PAGESIZEB bytes at a specific address in Flash
 *          Note: the bootcode itself is protected by the fuse map
 * In:      Command string
 * Out:     None
 ************************************************************/

static void cmdWriteBlock ( char *cmd )
{
    uint16_t addr;
    uint16_t i;
    uint8_t byte;
    
    // Format of command: WRBLK XXXX=YYYYYYYYYYYY...
    
    // Get address
    addr = DecodeHex16 ( &cmd[6] );
    
    // Decode all the bytes into the flash page buffer in RAM
    for ( i=0; i<SPM_PAGESIZE; i++ ) RXBuf[i] = DecodeHex8 ( &cmd[11+i*2] );

    // Perform the programming
    SPM_ProgPage ( (uint16_t)RXBuf, addr );

    // Check the page
    for ( i=0; i<SPM_PAGESIZE; i++, addr++ ) {
        byte = pgm_read_byte ( addr );
        if ( byte != RXBuf[i] ) break;
    }

    // Signal success
    if ( i < SPM_PAGESIZE )
            cmdset_SendReply ( "?" );
    else    cmdset_SendReply ( "#" );
}

/*************************************************************
 * Desc:    Write a CRC in the upper-most Flash page
 * In:      Command string
 * Out:     None
 ************************************************************/

static void cmdWriteCRC ( char *cmd )
{
    uint16_t crc, start, end;
    uint16_t addr;

    // Format of command: WRCRC CCCC,SSSS,EEEE

    // Decode the 3 16-bit values
    crc = DecodeHex16 ( &cmd[6] );
    start = DecodeHex16 ( &cmd[11] );
    end = DecodeHex16 ( &cmd[16] );

    // Prepare a flash buffer in RAM
    Reply[0] = crc & 0xFF;
    Reply[1] = crc >> 8;
    Reply[2] = start & 0xFF;
    Reply[3] = start >> 8;
    Reply[4] = end & 0xFF;
    Reply[5] = end >> 8;
    
    // Perform the programming
    addr = BOOTCODE_ADDR - SPM_PAGESIZE;
    SPM_ProgPage ( (uint16_t)Reply, addr );

    // Signal success
    cmdset_SendReply ( "#" );
}

/*************************************************************
 * Desc:    Initializes the commandset SPI interface
 * In:      None
 * Out:     None
 ************************************************************/

void cmdset_Init ( void )
{
    // SPI Slave initialization
    // Set MISO output, all others inputs
    CBI ( DDRB, SSP0_CELL_CS );
    CBI ( DDRB, SPI_DATA_MOSI );
    SBI ( DDRB, SPI_DATA_MISO );
    CBI ( DDRB, SPI_SCK );

    // Enable SPI
    // STR912 SPI master is configured for 8 bits, CPOL=1, CPHA=1, MSBit first
    // SPIE = 1 to enable interrupt
    // SPE = 1 to enable SPI
    // DORD = 0 for MSBit transmittted first
    // MSTR = 0 to select SPI slave mode
    // CPOL = 1
    // CPHA = 1
    // SPR1 = no effect on SPI slave
    // SPR0 = no effect on SPI slave
    SPCR = 0;
    SPCR = (1 << SPIE) | (1 << SPE) | (1 << CPOL) | (1 << CPHA);

    // Set some variables
    CommandReady = FALSE;
    RXBufInx = 0;
    TXBufInx = 0;
    Direction = SPI_RECEIVING;
}

/*************************************************************
 * Desc:    command set interpreter
 * In:      None
 * Out:     None
 ************************************************************/

void cmdset_Interface ( void )
{    
uint8_t lValidCmd = TRUE;
static uint8_t lDoCleanUpOnSSN = TRUE;
    
    // Check for incoming command
    if ( CommandReady ) {
        
        // Make the command uppercase
        // GFOURNIER 2009-01-19 : Now uppercasing right in ISR to save code space
        // strupr ( RXBuf );
        
        // Command must not be empty 
        if ( RXBufInx > 0 ) {
            
            if ( strncmp_P ( RXBuf, PSTR("BOOT"), 4 ) == 0 ) cmdBoot ( RXBuf );
            else if ( strncmp_P ( RXBuf, PSTR("RDPID"), 5 ) == 0 ) cmdReadPID ( RXBuf );
            else if ( strncmp_P ( RXBuf, PSTR("RUN"), 3 ) == 0 ) StartApp();
            else if ( strncmp_P ( RXBuf, PSTR("WRBLK"), 5 ) == 0 ) cmdWriteBlock ( RXBuf );
            else if ( strncmp_P ( RXBuf, PSTR("WRCRC"), 5 ) == 0 ) cmdWriteCRC ( RXBuf );
            else {
                cmdset_SendReply ( "INVALID" );
                lValidCmd = FALSE;
            }
            
            if (lValidCmd) StayInBoot = TRUE;
        }
        
        // Reset the index counter and the flag
        RXBufInx = 0;
        CommandReady = FALSE;
    }
    
    // Clean up state machine
    if ((PINB & (1 << SSP0_CELL_CS)) == 0) {
        // SSN is low (transfer is in progress)
        // Flag cleanup to be done as soon as SSN goes up
        lDoCleanUpOnSSN = TRUE;
    }
    else if (lDoCleanUpOnSSN) {
        // SSN is high (not transferring)
        // If the clean up was not done, do it now so next time SSN goes
        // low, we're going to be fresh and clean.  Typically, this
        // happens around a rising edge of SSN.
        cmdset_Init();
        lDoCleanUpOnSSN = FALSE;
    }
    
}

