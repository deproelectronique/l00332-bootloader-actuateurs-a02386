
/****************************************************************************
 *
 *                        D�pro �lectronique Inc.
 *                       755 Cur� Boivin, suite 101
 *                        Boisbriand, QC, Canada
 *
 *                (c) Copyright 2008, D�pro �lectronique Inc.
 *
 * Tous droits r�serv�s. Ce fichier est une propri�t� intellectuelle
 * et confidentielle de D�pro �lectronique Inc. Toute tentative
 * d'appropriation de ce fichier est strictement interdite sans le
 * consentement explicite de D�pro �lectronique.
 *
 * Projet :         L00259-BIN-R2.0.2
 *                  L00261-BIN-R2.0.2
 * Fichier :        SelfProg.c
 * Programmeur(S) : Stephane Pigeon
 *                  
 * Date :           2008/09/30
 * IDE :            WinAVR 20071221
 *                 
 * Description :    
 *        
 * Self-flash-programming using the SPM instruction
 * 
 ****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

#include "SelfProg.h"

#define PAGESIZEBLO (SPM_PAGESIZE&0xFF)
#define PAGESIZEBHI (SPM_PAGESIZE/256)

#define _SELFPRGEN  0       // Self programming enable: i need to redefine this because
                            // SELFPRGEN has 2 distinct definitions in iomx8.h and iom168.h

/*************************************************************
 * Desc:    Self Program
 *          This routine writes one page of data from RAM to Flash
 *          Note: the bootcode itself is protected by the fuse map
 * In:      from = RAM data pointer, 16-bit address
 *          to = FLASH page pointer, 16-bit address
 * Out:     '#' if success
 *          '?' if failure
 ************************************************************/

void SPM_ProgPage ( register uint16_t from, register uint16_t to )
{
    // **************************************************************
    //
    // This is done in assembly for a number of reasons: 
    // - To perform the SPM instruction
    // - To use the specific registers required by SPM
    // - To achieve strict timings for the SPM instruction
    // - The example in the user manual was in assembly ! :-)
    //
    // Function returns the X register.
    //
    // **************************************************************
   
    register uint8_t R0 asm("r0");          // Needed by instruction SPM
    register uint8_t R1 asm("r1");
    register uint8_t temp1 asm("r16");      // Temporary hold to check statuses in register SPMCSR
    register uint8_t temp2 asm("r17");      // Temporary hold for register SREG
    register uint8_t spmcsrval asm("r20");  // Parmeter to routine SPM_PEROFORM (holds value to write to register SPMCSR)
    register uint8_t looplo asm("r24");     // Self-programming loop counter / return value
    register uint8_t loophi asm("r25");
    register uint16_t Y asm("r28");         // Y register, points to RAM data
    register uint8_t ZL asm("r30");         // Z register, points to FLASH pages
    register uint8_t ZH asm("r31");
    
    // **************************************************************
    // For some reasons, the compiler does not protect some registers 
    // forcibly assigned to variables. Let's protect them manually.
    // **************************************************************
    asm volatile ( " push r0" );
    asm volatile ( " push r1" );
    // asm volatile ( " push r16" );    // Protected by compiler
    // asm volatile ( " push r17" );
    asm volatile ( " push r20" );
    // asm volatile ( " push r24" );    // Don't protect - holds the function's return value
    // asm volatile ( " push r25" );
    // asm volatile ( " push r28" );    // Protected by compiler
    // asm volatile ( " push r29" );
    asm volatile ( " push r30" );
    asm volatile ( " push r31" );

    // **************************************************************
    // Setup the Y pointer register to the address of the data in RAM.
    // The Y pointer will be used in the loop to send the data to the 
    // Flash page buffer.
    // **************************************************************
    asm volatile ( " movw   %0, %1" : "=r" (Y) : "r" (from) );

    // **************************************************************
    // Setup the Z pointer register to the address of the page in Flash.
    // **************************************************************
    asm volatile ( " movw   %0, %1" : "=r" (ZL) : "r" (to) );

    // **************************************************************
    // Perform SPM page erase operation @ address specified by the Z 
    // pointer register.
    // **************************************************************
    asm volatile ( " ldi    %0, %1" : "=r" (spmcsrval) : "M" ((1<<PGERS)|(1<<_SELFPRGEN)) );
    asm volatile ( " rcall  SPM_PERFORM" );

    // **************************************************************
    // Re-enable the RWW section, because it was temporarily disabled 
    // by the page erase operation.
    // **************************************************************
    asm volatile ( " ldi    %0, %1" : "=r" (spmcsrval) : "M" ((1<<RWWSRE)|(1<<_SELFPRGEN)) );
    asm volatile ( " rcall  SPM_PERFORM" );

    // **************************************************************
    // Loop: transfer data from RAM to Flash page buffer.
    // **************************************************************
    asm volatile ( " ldi    %0, %1" : "=r" (looplo) : "M" (PAGESIZEBLO) );
    asm volatile ( " ldi    %0, %1" : "=r" (loophi) : "M" (PAGESIZEBHI) );
    asm volatile ( "SPM_WRLOOP:" );
    asm volatile ( " wdr" );
    asm volatile ( " ld %0, Y+" : "=r" (R0) );
    asm volatile ( " ld %0, Y+" : "=r" (R1) );
    asm volatile ( " ldi    %0, %1" : "=r" (spmcsrval) : "M" (1<<_SELFPRGEN) );
    asm volatile ( " rcall  SPM_PERFORM" );
    asm volatile ( " adiw   %0, 2" : "=r" (ZL) );
    asm volatile ( " sbiw   %0, 2" : "=r" (looplo) );
    asm volatile ( " brne   SPM_WRLOOP" );

    // **************************************************************
    // Execute page-write operarion.
    // **************************************************************
    asm volatile ( " subi   %0, %1" : "=r" (ZL) : "M" (PAGESIZEBLO) );
    asm volatile ( " sbci   %0, %1": "=r" (ZH) : "M" (PAGESIZEBHI) );
    asm volatile ( " ldi    %0, %1" : "=r" (spmcsrval) : "M" ((1<<PGWRT)|(1<<_SELFPRGEN)) );
    asm volatile ( " rcall  SPM_PERFORM" );

    // **************************************************************
    // Re-enable the RWW section, because it was temporarily disabled 
    // by the page erase operation.
    // **************************************************************
    asm volatile ( " ldi    %0, %1" : "=r" (spmcsrval) : "M" ((1<<RWWSRE)|(1<<_SELFPRGEN)) );
    asm volatile ( " rcall  SPM_PERFORM" );

    // **************************************************************
    // Don't read back the Flash page, let the caller handle this.
    // **************************************************************
    
    // **************************************************************
    // We're done, make sure RWW section is safe to read, then leave.
    // **************************************************************
    asm volatile ( "SPM_RETURN:" );
    asm volatile ( " in     %0, %1" : "=r" (temp1) : "I" (_SFR_IO_ADDR(SPMCSR)) );
    asm volatile ( " sbrs   %0, %1" : : "r" (temp1) , "I" (RWWSB) );
    asm volatile ( " rjmp   SPM_DONE" );
    asm volatile ( " ldi    %0, %1" : "=r" (spmcsrval) : "M" ((1<<RWWSRE)|(1<<_SELFPRGEN)) );
    asm volatile ( " rcall  SPM_PERFORM" );
    asm volatile ( " rjmp   SPM_RETURN" );

    // **************************************************************
    //
    // SPM_PERFORM - this routine performs an actual SPM operation, 
    //               according to the bitfield held in spmcsrval.
    //
    // **************************************************************

    asm volatile ( "SPM_PERFORM:" );
    // Check for previous SPM operation complete.
    asm volatile ( "SPM_WAIT:" );
    asm volatile ( " in     %0, %1" : "=r" (temp1) : "I" (_SFR_IO_ADDR(SPMCSR)) );
    asm volatile ( " sbrc   %0, %1" : : "r" (temp1) , "I" (_SELFPRGEN) );
    asm volatile ( " rjmp   SPM_WAIT" );
    // Store status register and disable interrupts.
    asm volatile ( " in     %0, __SREG__" : "=r" (temp2) );
    asm volatile ( " cli" );
    // Check that no EEPROM write access is active.
    asm volatile ( "SPM_WAIT_EE:" );
    asm volatile ( " sbic   %0, %1" : : "I" (_SFR_IO_ADDR(EECR)) , "I" (EEPE) );
    asm volatile ( " rjmp   SPM_WAIT_EE" );
    // SPM timed sequence, spmcsrval determines SPM action.
    asm volatile ( " out    %1, %0" : : "r" (spmcsrval), "I" (_SFR_IO_ADDR(SPMCSR)) );
    asm volatile ( " spm" );
    // restore SREG (also reenables interrupts *if* they were enabled before).
    asm volatile ( " out    __SREG__, %0" : : "r" (temp2) );
    asm volatile ( " ret" );

    // **************************************************************
    //
    // Bye bye - restore registers left unprotected by the compiler.
    //
    // **************************************************************
    
    asm volatile ( "SPM_DONE:" );
    asm volatile ( " pop r31" );
    asm volatile ( " pop r30" );
    // asm volatile ( " pop r29" );
    // asm volatile ( " pop r28" );
    // asm volatile ( " pop r25" );
    // asm volatile ( " pop r24" );
    asm volatile ( " pop r20" );
    // asm volatile ( " pop r17" );
    // asm volatile ( " pop r16" );
    asm volatile ( " pop r1" );
    asm volatile ( " pop r0" );
}       

