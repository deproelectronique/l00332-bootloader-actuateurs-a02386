
/****************************************************************************
 *
 *                        D�pro �lectronique Inc.
 *                       755 Cur� Boivin, suite 101
 *                        Boisbriand, QC, Canada
 *
 *                (c) Copyright 2008, D�pro �lectronique Inc.
 *
 * Tous droits r�serv�s. Ce fichier est une propri�t� intellectuelle
 * et confidentielle de D�pro �lectronique Inc. Toute tentative
 * d'appropriation de ce fichier est strictement interdite sans le
 * consentement explicite de D�pro �lectronique.
 *
 * Projet :         L00255-BIN-R2.0.2
 * Fichier :        adc.c
 * Programmeur(S) : Stephane Pigeon
 *                  
 * Date :           2008/10/01
 * IDE :            WinAVR 20071221
 *                 
 * Description :    
 *        
 *  Fonctions pour le ADC interne du ATmega168
 *
 *  Ce logiciel contr�le le bloc d'alimentation d�velopp� � l'interne par 
 *  D�pro �lectronique pour Calisto-F de Morgan Schaffer. L'objectif est
 *  de fournir une sortie 13.2V aux divers circuits composant Calisto-F,
 *  et de contr�ler 2 sorties de puissance pour des drivers Peltiers avec 
 *  des consignes en tension ou en courant.
 *
 *  C'est la carte CPU principale de Calisto-F qui envoie les consignes au
 *  logiciel du bloc d'alimentation, via une interface de contr�le SPI.
 * 
 ****************************************************************************/

/****************************************************************************
 *                                  INCLUDES
 ****************************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h>

#include "main.h"
#include "adc.h"

/****************************************************************************
 *                                  DATA TYPES
 ****************************************************************************/

/****************************************************************************
 *                                  PROTOTYPES
 ****************************************************************************/

/****************************************************************************
 *                                GLOBAL VARIABLEs
 ****************************************************************************/

/****************************************************************************
 *                                PRIVATE VARIABLES
 ****************************************************************************/

static uint8_t ConvReady;

/****************************************************************************
 * Desc:    Interruption pour la fin de la conversion du ADC
 * In:      None
 * Out:     None
 ****************************************************************************/

SIGNAL(SIG_ADC)
{
    ConvReady = TRUE;
}

/****************************************************************************
 * Desc:    Fonction qui initialise le convertisseur AD
 * In:      None
 * Out:     None
 ****************************************************************************/

void adc_Init(void)
{
    ConvReady = FALSE;

    // Registre DIRDR0: valeur initiale: 0x00
    // les canaux ADC6 et ADC7 sont d�di�s au convertisseur AD,
    // donc n'ont pas � �tre configur�s
    // Configure les I/O PC5 (tempBox)
    SBI(DIDR0,ADC5D);
    
    // Registre ADMUX: valeur initiale: 0x00
    // R�sultat justifi� � droite
    // Configure la r�f�rence � AVCC
    SBI(ADMUX,REFS0);
    
    // Registre ADCSRA: valeur initiale: 0x00
    // Autotrigger d�sactiv�: rien a configuer dans le registre ADCSRB
    // Interruption activ�e
    ADCSRA = (1<<ADIE) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0);
    //SBI(ADCSRA,ADIE);
    // Prescaler: Fosc / 128
    //SBI(ADCSRA,ADPS2);
    //SBI(ADCSRA,ADPS1);
    //SBI(ADCSRA,ADPS0);
    
    // Selectionne le canal
    ADMUX = 5;
    //CBI(ADMUX,MUX3);
    //SBI(ADMUX,MUX2);
    //CBI(ADMUX,MUX1);
    //SBI(ADMUX,MUX0);

    // Premi�re conversion
    // Active le convertisseur
    SBI(ADCSRA,ADEN);
}

/****************************************************************************
 * Desc:    Fonction qui g�re le convertisseur AD
 * In:      None
 * Out:     None
 ****************************************************************************/

char adc_Handle(uint16_t *adc_TempBox)
{
    uint8_t Low, High;
    
    // Si une conversion est termin�e
    if ( ConvReady ) {

        // Reset la condition
        ConvReady = FALSE;

        // Lit les deux registres du convertisseur
        Low = ADCL;
        High = ADCH;
        *adc_TempBox = High;
        *adc_TempBox = (*adc_TempBox << 8) + Low;

        // ADC 5: TEMPBOX
        //CBI(ADMUX,MUX3);
        //SBI(ADMUX,MUX2);
        //CBI(ADMUX,MUX1);
        //SBI(ADMUX,MUX0);
        
        // D�marre prochaine la conversion
        SBI(ADCSRA,ADSC);
        
        return TRUE;
    }
    return FALSE;
}

